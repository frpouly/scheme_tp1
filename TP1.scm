(define carre
  (lambda(n) (* n n)))

(carre 6)

(let* ((x 2)) (carre x))

(define quad
  (lambda(x)
    (let ((y (carre x))) (* y y))))


(define cercle
  (lambda(r)
    (list 'circonference (* 2 3.14 r) 'aire (* 3.14 r r))))

(cercle 2)

(define LAEI
       '(1 (2(6)(7))
           (3 (8 (12))(9))
           (4 (10(13)
                 (14(17)))
              (11 (15 (16)))
              (18))
           (5)))

; recup la feuille 14 et lui ajouter une feuille ((17)(19))

(let* ((racine (car LAEI))
       (lssa (cdr LAEI))
       (pssa (cadr LAEI))
       (dssa (caddr LAEI))
       (tssa (cadddr LAEI))
       (rssa (cddddr LAEI))
       (ractssa (car tssa))
       (sadix (cadr tssa))
       (saonze (caddr tssa))
       (dix (car sadix))
       (satreize (cadr sadix))
       (saquatorze (caddr sadix))
       (quatorze (car saquatorze))
       (lsaquatorze (cdr saquatorze)))
  (append (list racine
        pssa
        dssa
        (list ractssa
              (list dix satreize (cons quatorze (append lsaquatorze
                                                        (list '(19)))))
              saonze))
  rssa))


(define fac2
  (lambda(n)          
    (if (= 0 n)
        1
        (* n (fac2 (- n 1))))))

(define fac
  (lambda(n)
    (if (and (integer? n)
             (>= n 0))
        (fac2 n)
        'erreur)))

(fac 25)

(define somme_int
  (lambda(n)
    (if ( = 0 n)
        0
        (+ n (somme_int (- n 1))))))

(somme_int 4)

(define long
  (lambda(L)
    (if (null? L)
        0
        (+ 1 (long (cdr L))))))

(long '(1 2 3))

(define renverse
  (lambda(L)
    (if (null? L)
        '()
        (append (renverse (cdr  L)) (list (car L))))))

(define miroir
  (lambda(L)
    (if (null? L)
        '()
        (append (miroir (cdr  L)) (if (list? (car L))
                                      (list (miroir (car L)))
                                      (list (car L)))))))

(miroir '(1 ((2 3) 4) 3))
(miroir '(1 (2 3) 3))

(define carreL
  (lambda(L)
    (if (null? L)
        '()
        (append (list (carre (car L))) (carreL (cdr L))))))

(carreL '(1 2 3))

(define nbpos
  (lambda(L)
    (if (null? L)
        0
        (if (>= (car L) 0)
            (+ 1 (nbpos (cdr L)))
            (nbpos (cdr L))))))

(nbpos '(1 -2 3))

(define membre
  (lambda(x L)
    (if (null? L)
        #f
        (if (= (car L) x)
            #t
            (membre x (cdr L))))))

(membre 9 '(1 -2 3))

(define epure
  (lambda(L)
    (if (null? L)
        '()
        (if (not (membre (car L) (epure (cdr L))))
	    (cons (car L) (epure (cdr L)))
	    (epure (cdr L))))))

(epure '(2 6 4 3 2 9 6))

(define nieme
  (lambda(n L)
    (if (= 1 n)
	(car L)
	(nieme (- n 1) (cdr L)))))
(nieme 6 '(2 6 7 8 9 4 5 5))

(define insere
  (lambda(n x L)
    (if (null? L)
	'()
	(if (< 1 n)
	    (cons (car L) (insere (- n 1) x (cdr L)))
	    (cons x L)))))

(insere 2 4 '(1 5 6 9 7))

(define union
  (lambda(L1 L2)
    (epure (append L1 L2))))

(union '(1 2 3 5 6) '(5 5 5 5 5 5 7))

(define inter
  (lambda(L1 L2)
    (if (null? L1)
	'()
	(if (membre (car L1) L2)
	    (cons (car L1) (inter (cdr L1) L2))
	    (inter (cdr L1) L2)))))

(inter '(1 2 3 5 6) '(5 6 7 8))

(miroir '(1 2 3))

(define niv0
  (lambda(L)
    (if (null? L)
        '()
        (if (list? (car L))
            (append (niv0 (car L)) (niv0(cdr L)))
            (cons (car L) (niv0(cdr L)))))))

(niv0 '(2 (5 4 7) 1 (5 (7))))

(define zip
  (lambda(L1 L2)
    (if (or (null? L1) (null? L2))
        '()
        (cons (cons (car L1) (car L2)) (zip (cdr L1) (cdr L2))))))

(zip '(1 2 2 5 6) '(5 9 4 25 4 5 7 2))

(define prod2
  (lambda(L1 L2)
    (if (null? L2)
        0
        (+ (* (car L1) (car L2)) (prod (cdr L1) (cdr L2))))))

(define prod
  (lambda(L1 L2)
    (if (not (= (long L1) (long L2)))
        'erreur
        (prod2 L1 L2))))

(prod '(1 2 2 5 6) '(5 9 4 25 4 ))

(define som_list2
  (lambda (L)
    (if (null? L)
        #f
        (let ((res (som_list2 (cdr L))))
        (if (number? (car L))
            (if (number? res)
                (+ (car L) res)
                (car L))
            res)))))
                

(define som_list
  (lambda (L)
    (let ((res (som_list2 L)))
    (if (number? res)
        res
        'erreur))))

(som_list '(1 2 3 4))
(som_list '(1 1 a 5))
(som_list '(a v))

; Applique une fonction à tous les éléments de la liste L
(define mapkar
  (lambda (f L)
    (if (null? L)
        '()
        (cons (f (car L)) (mapkar f (cdr L))))))

(mapkar sqrt '(2 9 81))



    
(restart 1)





